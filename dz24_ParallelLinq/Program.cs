﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ex24_ParallelLinq
{
    class Program
    {
        private static int masCount = 100_000;
        private static int[] mas;
        static int procCount = 4;
        static string FuncName;
        static void Main(string[] args)
        {

            //FirstStart();
            //ThreadTest();
            //TasksTest();
            //TaskRunTest();
            //TasksCalcTest();
            //TasksCalc2Test();
            //ParallelForCalcTest();
            //ParallelLinqCalcTest();            

            //DzSimpleCalTest();
            //DzParallelCalTest();
            DzLinqlCalTest();
            Console.ReadKey();
        }

        public static void FirstStart()
        {
            /*Thread t = Thread.CurrentThread;
                Console.WriteLine($"Имя потока: {t.Name}");
                Console.WriteLine($"Статус потока: {t.ThreadState}");
                Console.WriteLine($"Домен приложения: {Thread.GetDomain().FriendlyName}");
                */
        }

        public static void ThreadTest()
        {
            Console.WriteLine("1="+Thread.CurrentThread.Name);
            Thread.CurrentThread.Name = "поток";
            Console.WriteLine("2=" + Thread.CurrentThread.Name);
            var thread = new Thread(new ThreadStart(() => Console.WriteLine("3="+Thread.CurrentThread.Name)));
            thread.Start();
        }

        public static void TasksTest()
        {
            Task finish = null;

            Task[] tasks =
            {
                new Task(()=>Console.WriteLine("Task 1")),
                new Task(()=>Console.WriteLine("Task 2")),
                new Task(()=>Console.WriteLine("Task 3")),
                new Task(()=>throw new Exception("Exception 1")),
                new Task(()=>throw new Exception("Exception 2"))
            };

            tasks[0].ContinueWith(t => Console.WriteLine("Task 1 Add"));

            try
            {
                finish = Task.WhenAll(tasks);
                foreach (var t in tasks)
                {
                    t.Start();
                }
                Task.WaitAll(tasks);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(string.Join("\n",finish.Exception.InnerExceptions));
            }
            Console.ReadKey();

        }

        public static void TaskRunTest()
        {
            Task task = Task.Run(() => Console.WriteLine("Hello task!"));

            Task task2 = Task.Factory.StartNew(() => Console.WriteLine("Hello task factary!"));

        }

        static void Calc(int i)
        {
            for (int j = 0; j < 100; j++)
            {
                mas[i] += 1;
            }
        }

        public static  int[] GenerateArray( int count)
        {
            int[] arr = new int[count];
            Random rand = new Random();
            for (int i = 0; i < count; i++)
            {
                arr[i] = rand.Next(1, 9);
            }
            return arr;
        }

        public static  void Show( int [] arr)
        {

            for (int i = 0; i < 10; i++)
            {
                Console.Write(arr[i]+",");
            }
        }

        static void TimeTest(Action act)
        {
            mas = GenerateArray(masCount);
            Show(mas);
            var timer = Stopwatch.StartNew();
            act();
            timer.Stop();
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine($"Функция: {FuncName};  Время: {timer.ElapsedMilliseconds} мс. Кол-во: {masCount} ; ");
            Console.WriteLine("");
            Show(mas);

        }

        public static void TasksCalcTest()
        {
            TimeTest(() =>
            {
                List<Task> tasks = new List<Task>();
                var n = mas.Length;
                var procCount = 4;
                for (int i = 0; i < n; i++)
                {
                    var i1 = i;
                    tasks.Add(new Task(() => Calc(i1)));
                }

                foreach (var t in tasks)
                {
                    t.Start();
                }
                Task.WaitAll(tasks.ToArray());
            }
            );
           
            Console.ReadKey();

        }
        public static void TasksCalc2Test()
        {
            FuncName = "TasksCalc2Test";
            TimeTest(() =>
            {
                List<Task> tasks = new List<Task>();
                var n = mas.Length;
                var procCount = 4;
                for (int i = 0; i < procCount; i++)
                {
                    var i1 = i;
                    tasks.Add(new Task(() => 
                    {
                        var min = i1 * n / procCount;
                        var max = (i1+1) * n / procCount;
                        for (int j = min; j < max; j++)
                        {
                            Calc(j);
                        }                        
                    }));
                }

                foreach (var t in tasks)
                {
                    t.Start();
                }
                Task.WaitAll(tasks.ToArray());
            }
            );

            Console.ReadKey();

        }

        public static void ParallelForCalcTest()
        {
            FuncName = "ParallelForCalcTest";
            TimeTest(() =>
            {
                Parallel.For(0, mas.Length, Calc);
            });
            Console.ReadKey();
        }
        public static void ParallelLinqCalcTest()
        {
            FuncName = "ParallelLinqCalcTest";


            TimeTest(() =>
            {
                mas.AsParallel().ForAll(Calc);
                
            });
            Console.ReadKey();
        }
        public static void DzSimpleCalTest()
        {
            FuncName = "SimpleCalTest";


            TimeTest(() =>
            {
                var sum = mas.Sum();

            });
            Console.ReadKey();

        }
        public static void DzParallelCalTest()
        {
            FuncName = "DzParallelCalTest";
            TimeTest(() =>
            {
                var sum = 0;
                Parallel.For(0, mas.Length, i => sum += mas[i]);

            });
            Console.ReadKey();

        }
        public static void DzLinqlCalTest()
        {
            FuncName = "DzLinqlCalTest";
            TimeTest(() =>
            {
                var sum = 0;
                mas.AsParallel().ForAll(i=>sum+=mas[i]);

            });
            Console.ReadKey();

        }

    }
}

